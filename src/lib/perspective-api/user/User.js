

module.exports = class User {
    constructor(data) {
        if (data.id !== undefined) {
            this.id = data.user_id
        }
        if (data.guild !== undefined) {
            this.guild = data.guild_id
        }
        if (data.toxicity !== undefined) {
            this.toxicity = data.toxicity || 0.00
        }
    }
}