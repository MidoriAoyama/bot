


module.exports = {
    'createInstantInvite': 1 << 0,
    'kickMembers': 1 << 1,
    'banMembeers': 1 << 2,
    'administrator': 1 << 3,
    'manageChannels': 1 << 4,
    'manageGuild': 1 << 5,
    'addReactions': 1 << 6,
    'viewAuditLog': 1 << 7,
    'prioritySpeaker': 1 << 8,
    'stream': 1 << 9,
    'viewChannel': 1 << 10,
    'sendMessages': 1 << 11,
    'sendTTSMessage': 1 << 12,
    'manageChannels': 1 << 13,
    'embedLinks': 1 << 14,
    'attachFiles': 1 << 15,
    'readMessageHistory': 1 << 16,
    'mentionEveryone': 1 << 17,
    'useExternalEmojis': 1 << 18,
    'viewGuildInsights': 1 << 19,
    'connect': 1 << 20,
    'speak': 1 << 21,
    'muteMembers': 1 << 22,
    'deafenMembers': 1 << 23,
    'moveMembers': 1 << 24,
    'useVad': 1 << 25,
    'changeNickname': 1 << 26,
    'manageNicknames': 1 << 27,
    'manageRoles': 1 << 28,
    'manageWebhooks': 1 << 29,
    'useSlashCommands': 1 << 32,
    'requestToSpeak': 1 << 31,
    'manageEmojis': 1 << 30,
    'manageThreads': 1 << 34,
    'usePublicThreads': 1 << 35,
    'usePrivateThreads': 1 << 36,
}