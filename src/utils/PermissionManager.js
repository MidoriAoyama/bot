const { PermissionBit, PermissionStore } = require('../store/Store')
const Store = require('../store/Store')


module.exports = class PermissionManager {
    calculate(bits) {
        const permissions = []
        for (let permission in PermissionStore) {
            if ((BigInt(PermissionBit[permission]) & BigInt(bits)) == BigInt(PermissionBit[permission])) {
                permissions.push(permission)
            }
        }
        

        return permissions
    }
}